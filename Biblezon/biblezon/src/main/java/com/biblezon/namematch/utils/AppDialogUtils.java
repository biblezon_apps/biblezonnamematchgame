package com.biblezon.namematch.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.BadTokenException;
import android.widget.Button;
import android.widget.TextView;

import com.biblezon.namematch.R;
import com.biblezon.namematch.ihelper.AlertDialogClickListener;

public class AppDialogUtils {
	Activity mActivity;
	private static Dialog dialog;

	/**
	 * Show Message Info View
	 * 
	 * @param mActivity
	 * @param msg
	 */
	@SuppressLint({ "InflateParams", "DefaultLocale" })
	public static void showMessageInfoWithOkButtonDialog(Activity mActivity,
			String msg, final AlertDialogClickListener mAlertDialogClickListener) {
		/* Check if Alert is already there so remove old one */
		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
		// Create custom dialog object
		if (mActivity != null) {
			dialog = new Dialog(mActivity);
			// hide to default title for Dialog
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			// inflate the layout dialog_layout.xml and set it as contentView
			LayoutInflater inflater = (LayoutInflater) mActivity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view = inflater.inflate(R.layout.text_button_dialog, null,
					false);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setContentView(view);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

			// Retrieve views from the inflated dialog layout and update their
			// values
			TextView messageText = (TextView) dialog.findViewById(R.id.msgText);

			messageText.setText(msg);

			Button okButtontextDialog = (Button) dialog
					.findViewById(R.id.okButtontextDialog);
			okButtontextDialog.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// Dismiss the dialog
					dialog.dismiss();
					if (mAlertDialogClickListener != null) {
						mAlertDialogClickListener
								.onClickOfAlertDialogPositive();
					}

				}
			});
			try {

				// Display the dialog
				dialog.show();
			} catch (BadTokenException e) {
				// TODO: handle exception
				e.printStackTrace();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

}
