package com.biblezon.namematch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.biblezon.namematch.control.HeaderViewManager;
import com.biblezon.namematch.control.MediaControlManager;
import com.biblezon.namematch.control.PlaySound;
import com.biblezon.namematch.ihelper.AlertDialogClickListener;
import com.biblezon.namematch.utils.AppDefaultUtils;
import com.biblezon.namematch.utils.AppDialogUtils;
import com.biblezon.namematch.utils.AppHelper;
import com.biblezon.namematch.utils.GlobalConfig;

/**
 * Tap It Game Activity
 * 
 * @author Shruti
 * 
 */
public class TapItActivity extends Activity {

	Activity mActivity;
	TextView letter_bin, game_count;
	/**
	 * current puzzle name
	 */

	String current_puzzle_name = "";
	List<String> new_string_array;

	/**
	 * total puzzle count
	 */
	int puzzle_count = GlobalConfig.no_of_question;
	int current_puzzle_index = 0;

	/**
	 * shaking animation
	 */
	Animation shake_anim, pulse;
	HashMap<String, Drawable> mResultHashMap;
	List<Integer> generated;

	/**
	 * debug tag for this activity
	 */
	String TAG = TapItActivity.class.getSimpleName();
	boolean isRedayToPlay = false;

	/**
	 * Array of Images and image view
	 */
	final int[] imageViews = { R.id.firstWordImage, R.id.secondWordImage,
			R.id.thirdWordImage, R.id.fourthWordImage, R.id.fifthWordImage,
			R.id.sixthWordImage, R.id.seventhWordImage, R.id.eigthWordImage,
			R.id.ninethWordImage, R.id.tenthWordImage, R.id.eleventhWordImage,
			R.id.twelthWordImage };

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tap_it_layout);
		initViews();
		getAllPuzzleWord();
		headerViewManagement();
		updateCurrentPuzzleView();
		assignClickToImageView();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		PlaySound.getInstance(this).startPlay();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		PlaySound.getInstance(this).stopPlay();
	}

	/**
	 * initializes views for this activity
	 */
	private void initViews() {
		mActivity = this;
		letter_bin = (TextView) findViewById(R.id.letter_bin);
		game_count = (TextView) findViewById(R.id.game_count);
		game_count.setText("1");
		isRedayToPlay = true;
		shake_anim = AnimationUtils.loadAnimation(this, R.anim.shake_anim);
		pulse = AnimationUtils.loadAnimation(this, R.anim.pulse_anim);
	}

	/**
	 * assign clicks to the imageview present in tap it screen
	 */
	private void assignClickToImageView() {
		for (int i = 0; i < 12; i++) {
			ImageView iv = (ImageView) findViewById(imageViews[i]);
			iv.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (isRedayToPlay) {
						ImageView imageView = (ImageView) v;
						AppDefaultUtils.showLog(TAG, "imageView.getTag() : "
								+ imageView.getTag());
						MediaControlManager.getInstance(mActivity).startPlay(
								imageView.getTag().toString());
						if (current_puzzle_name
								.equalsIgnoreCase((String) imageView.getTag())) {
							isRedayToPlay = false;
							imageView.startAnimation(pulse);
							new Handler().postDelayed(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									MediaControlManager.getInstance(mActivity)
											.startPlay("good_job");
								}
							}, AppHelper.ONE_SECOND_TIME * 2);
							new Handler().postDelayed(new Runnable() {

								@Override
								public void run() {
									isRedayToPlay = true;
									updateCurrentPuzzleView();
								}
							}, AppHelper.ONE_SECOND_TIME * 4);

						} else {
							imageView.startAnimation(shake_anim);
						}
					}
				}
			});
		}
	}

	/**
	 * manages header view
	 */
	private void headerViewManagement() {
		HeaderViewManager.getInstance().InitializeHeaderView(mActivity);
		HeaderViewManager.getInstance().setHeading(true,
				mActivity.getResources().getString(R.string.tap_it));
	}

	/**
	 * update the current puzzle view
	 */
	private void updateCurrentPuzzleView() {
		if (current_puzzle_index == puzzle_count) {
			AppDialogUtils.showMessageInfoWithOkButtonDialog(
					TapItActivity.this, TapItActivity.this.getResources()
							.getString(R.string.onsuccess), onGameComplete());
			// Toast.makeText(mActivity,
			// "You have completed the game. yipeeee..... :-)",
			// Toast.LENGTH_LONG).show();
		} else {
			if (new_string_array != null
					&& new_string_array.size() > (current_puzzle_index)) {
				current_puzzle_name = new_string_array
						.get(current_puzzle_index);
				AppDefaultUtils.showLog(TAG, "current_puzzle_name : "
						+ current_puzzle_name);
				letter_bin.setText(current_puzzle_name.replaceAll("_", " ")
						.toUpperCase());
				current_puzzle_index = current_puzzle_index + 1;
				game_count.setText("" + current_puzzle_index);
			}
			showMapImagesOnGUI(current_puzzle_index - 1);
		}
	}

	/**
	 * Shuffle that original map
	 * 
	 * @param map
	 * @return
	 */
	public static <K, V> Map<K, V> shuffleMap(Map<K, V> map) {
		List<V> valueList = new ArrayList<V>(map.values());
		Collections.shuffle(valueList);
		Iterator<V> valueIt = valueList.iterator();
		Map<K, V> newMap = new HashMap<K, V>(map.size());
		for (K key : map.keySet()) {
			newMap.put(key, valueIt.next());
		}
		return newMap;
	}

	/**
	 * Show Images on GUI
	 */
	@SuppressLint("DefaultLocale")
	private void showMapImagesOnGUI(int current_index) {
		// Random rng = new Random();
		// generated = new ArrayList<Integer>();
		Log.d(TAG, "generated.size() : " + generated.size());
		List<Integer> gridIndexs = new ArrayList<Integer>();
		List<Integer> tempArray = new ArrayList<Integer>();
		tempArray.addAll(generated);
		gridIndexs.add(tempArray.get(current_index));
		int addedIndex = tempArray.get(current_index);
		tempArray.remove(current_index);
		Collections.shuffle(tempArray);
		for (int i = 0; i < 11; i++) {
			if (!tempArray.get(i).equals(addedIndex)) {
				gridIndexs.add(tempArray.get(i));
			}
		}
		Collections.shuffle(gridIndexs);
		for (int i = 0; i < gridIndexs.size(); i++) {
			// while (true) {
			// Integer next = rng.nextInt(12);
			// if (!generated.contains(next)) {
			// generated.add(next);
			ImageView iv = (ImageView) findViewById(imageViews[i]);
			iv.setImageBitmap(AppDefaultUtils
					.getImageFromStorage(AppHelper.catholicImagesArray[gridIndexs
							.get(i)]));
			iv.setTag(AppDefaultUtils
					.getImageTAGFromPath(
							AppHelper.catholicImagesArray[gridIndexs.get(i)])
					.toString()
					/* .replaceAll("_", " ") */.toUpperCase());
			// break;
			// }
			// }
		}
	}

	/**
	 * get all puzzle words according to the puzzle count
	 */
	public void getAllPuzzleWord() {
		new_string_array = new ArrayList<String>();
		generated = new ArrayList<Integer>();
		Random rng = new Random();
		if (puzzle_count < 12) {
			for (int i = 0; i < 12; i++) {
				while (true) {
					Integer next_value = rng
							.nextInt(AppHelper.sizeOfChatholicData);
					if (!generated.contains(next_value)) {
						generated.add(next_value);
						if (i < puzzle_count) {

							new_string_array
									.add(AppHelper.catholicNameArray[next_value]);

						}
						break;
					}
				}
			}
		} else {
			for (int i = 0; i < puzzle_count; i++) {
				while (true) {
					Integer next_value = rng
							.nextInt(AppHelper.sizeOfChatholicData);
					if (!generated.contains(next_value)) {
						generated.add(next_value);
						new_string_array
								.add(AppHelper.catholicNameArray[next_value]);
						break;
					}
				}
			}
		}
	}

	/**
	 * On Game Complete listner
	 * 
	 * @return
	 */
	private AlertDialogClickListener onGameComplete() {
		AlertDialogClickListener mListener = new AlertDialogClickListener() {

			@Override
			public void onClickOfAlertDialogPositive() {
				onBackPressed();
			}
		};
		return mListener;
	}

}
