package com.biblezon.namematch.utils;

public interface OnMovementListener {
	public abstract void onMovement();
}
