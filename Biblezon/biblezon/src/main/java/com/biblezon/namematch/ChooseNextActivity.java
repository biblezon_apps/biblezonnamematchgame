package com.biblezon.namematch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.biblezon.namematch.control.PlaySound;
import com.biblezon.namematch.memorymatch.MemoryMatchGameScreen;
import com.biblezon.namematch.utils.AppDefaultUtils;
import com.biblezon.namematch.utils.AppHelper;
import com.biblezon.namematch.utils.GlobalConfig;

/**
 * with this activity user can choose the next activity which the user want to
 * play
 * 
 * @author Shruti
 * 
 */
public class ChooseNextActivity extends Activity implements OnClickListener {

	/**
	 * textviews to choose options
	 */
	TextView name_match, memory_match, bible_order, spell_it, tap_it;
	Animation bounce;
	Activity mActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_next_activity_layout);
		initViews();
		assignClicks();
		AnimateActivityViews();
	}

	/**
	 * one by one animate the views
	 */
	private void AnimateActivityViews() {

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				name_match.setVisibility(View.VISIBLE);
				memory_match.setVisibility(View.VISIBLE);
				name_match.startAnimation(bounce);
				memory_match.startAnimation(bounce);
			}
		}, 800);

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				spell_it.setVisibility(View.VISIBLE);
				tap_it.setVisibility(View.VISIBLE);
				spell_it.startAnimation(bounce);
				tap_it.startAnimation(bounce);
			}
		}, 1600);

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				bible_order.setVisibility(View.VISIBLE);
				bible_order.startAnimation(bounce);
			}
		}, 2400);
	}

	/**
	 * assign clicks to the view fields
	 */
	private void assignClicks() {
		spell_it.setOnClickListener(this);
		tap_it.setOnClickListener(this);
		memory_match.setOnClickListener(this);
		name_match.setOnClickListener(this);
		bible_order.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		PlaySound.getInstance(mActivity).startPlay();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		PlaySound.getInstance(mActivity).stopPlay();
	}

	/**
	 * initalizes the views
	 */
	private void initViews() {
		mActivity = this;
		spell_it = (TextView) findViewById(R.id.spell_it);
		tap_it = (TextView) findViewById(R.id.tap_it);
		name_match = (TextView) findViewById(R.id.name_match);
		memory_match = (TextView) findViewById(R.id.memory_match);
		bible_order = (TextView) findViewById(R.id.bible_order);
		bounce = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.spell_it:
			if (AppHelper.catholicImagesArray.length == AppHelper.catholicNameArray.length) {
				GlobalConfig.NextActivity = AppHelper.SPELL_IT;
				startActivity(new Intent(ChooseNextActivity.this,
						NumberOfQuestionsActivity.class));
			} else {
				AppDefaultUtils.showToast(mActivity,
						"Loading Image Data Please Wait.");
			}

			break;
		case R.id.tap_it:
			if (AppHelper.catholicImagesArray.length == AppHelper.catholicNameArray.length) {
				GlobalConfig.NextActivity = AppHelper.TAP_IT;
				startActivity(new Intent(ChooseNextActivity.this,
						NumberOfQuestionsActivity.class));
			} else {
				AppDefaultUtils.showToast(mActivity,
						"Loading Image Data Please Wait.");
			}

			break;

		case R.id.name_match:
			if (AppHelper.catholicImagesArray.length == AppHelper.catholicNameArray.length) {
				startActivity(new Intent(ChooseNextActivity.this,
						NameMatchScreen.class));
			} else {
				AppDefaultUtils.showToast(mActivity,
						"Loading Image Data Please Wait.");
			}

			break;
		case R.id.memory_match:
			if (AppHelper.catholicImagesArray.length == AppHelper.catholicNameArray.length) {
				startActivity(new Intent(ChooseNextActivity.this,
						MemoryMatchGameScreen.class));
			} else {
				AppDefaultUtils.showToast(mActivity,
						"Loading Image Data Please Wait.");
			}

			break;
		case R.id.bible_order:
			// if (AppHelper.catholicImagesArray.length ==
			// AppHelper.catholicNameArray.length) {
			startActivity(new Intent(ChooseNextActivity.this,
					KidsSplashActivity.class));
			// } else {
			// AppDefaultUtils.showToast(mActivity,
			// "Loading Image Data Please Wait.");
			// }

			break;

		default:
			break;
		}
	}
}
