package com.biblezon.namematch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.biblezon.namematch.control.HeaderViewManager;
import com.biblezon.namematch.control.PlaySound;
import com.biblezon.namematch.utils.AppHelper;
import com.biblezon.namematch.utils.GlobalConfig;

/**
 * with this activity user can choose the number of questions
 * 
 * @author Shruti
 * 
 */
public class NumberOfQuestionsActivity extends Activity implements
		OnClickListener {

	/**
	 * textviews to choose options
	 */
	TextView five, ten, fifteen, twenty, twenty_five, title;
	Animation zoomin;
	Activity mActivity;
	String biblezon = "BibleZon";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.number_of_questions);
		initViews();
		changeHeaderText();
		assignClicks();
		headerViewManagement();
		AnimateActivityViews();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		PlaySound.getInstance(mActivity).startPlay();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		PlaySound.getInstance(mActivity).stopPlay();
	}

	/**
	 * one by one animate the views
	 */
	private void AnimateActivityViews() {
		five.startAnimation(zoomin);
		ten.startAnimation(zoomin);
		fifteen.startAnimation(zoomin);
		twenty.startAnimation(zoomin);
		twenty_five.startAnimation(zoomin);
	}

	/**
	 * manages header view
	 */
	private void headerViewManagement() {
		HeaderViewManager.getInstance().InitializeHeaderView(mActivity);
		HeaderViewManager.getInstance().setHeading(false, "");
	}

	/**
	 * assign clicks to the view fields
	 */
	private void assignClicks() {
		five.setOnClickListener(this);
		ten.setOnClickListener(this);
		fifteen.setOnClickListener(this);
		twenty.setOnClickListener(this);
		twenty_five.setOnClickListener(this);
	}

	/**
	 * initalizes the views
	 */
	private void initViews() {
		mActivity = this;
		five = (TextView) findViewById(R.id.five);
		ten = (TextView) findViewById(R.id.ten);
		fifteen = (TextView) findViewById(R.id.fifteen);
		twenty = (TextView) findViewById(R.id.twenty);
		twenty_five = (TextView) findViewById(R.id.twenty_five);
		title = (TextView) findViewById(R.id.title);
		zoomin = AnimationUtils.loadAnimation(mActivity, R.anim.zoom_in);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.five:
			GlobalConfig.no_of_question = 5;
			callNextActivity();
			break;
		case R.id.ten:
			GlobalConfig.no_of_question = 10;
			callNextActivity();
			break;

		case R.id.fifteen:
			GlobalConfig.no_of_question = 15;
			callNextActivity();
			break;
		case R.id.twenty:
			GlobalConfig.no_of_question = 20;
			callNextActivity();
			break;
		case R.id.twenty_five:
			GlobalConfig.no_of_question = 25;
			callNextActivity();
			break;

		default:
			break;
		}
	}

	/**
	 * calls the next activity
	 */
	private void callNextActivity() {

		if (GlobalConfig.NextActivity.equalsIgnoreCase(AppHelper.SPELL_IT)) {
			startActivity(new Intent(NumberOfQuestionsActivity.this,
					SpellItActivity.class));
		} else if (GlobalConfig.NextActivity.equalsIgnoreCase(AppHelper.TAP_IT)) {
			startActivity(new Intent(NumberOfQuestionsActivity.this,
					TapItActivity.class));
		}
	}

	/**
	 * change header text according to the selected game
	 */
	private void changeHeaderText() {

		if (GlobalConfig.NextActivity.equalsIgnoreCase(AppHelper.SPELL_IT)) {
			title.setText(biblezon + "\n" + AppHelper.SPELL_IT);
		} else if (GlobalConfig.NextActivity.equalsIgnoreCase(AppHelper.TAP_IT)) {
			title.setText(biblezon + "\n" + AppHelper.TAP_IT);
		}
	}
}
