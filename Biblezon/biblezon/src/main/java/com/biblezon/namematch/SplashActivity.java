package com.biblezon.namematch;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.biblezon.namematch.utils.AppDefaultUtils;
import com.biblezon.namematch.utils.AppHelper;
import com.biblezon.namematch.webservice.MemoryMatchAPIHandler;
import com.biblezon.namematch.webservice.VersionCheckAPIHandler;
import com.biblezon.namematch.webservice.control.WebAPIResponseListener;

/**
 * Splash activity will be visible to the user as the first activity
 *
 * @author Anshuman
 */
public class SplashActivity extends Activity {
    /**
     * Debugging TAG
     */
    String TAG = SplashActivity.class.getSimpleName();
    Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        mActivity = this;
        new VersionCheckAPIHandler(mActivity, webAPIResponseLinsener());

    }

    /**
     * Splash Animation
     */
    private void SplashAnimation() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // GetGameDataFromServer();
                saveAllDrawableToStorage();
                moveToNextScreen();
            }

        }, 2000);
    }

    /**
     * Mass Base Response Listener
     */
    private WebAPIResponseListener webAPIResponseLinsener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onSuccessOfResponse(Object... arguments) {
                SplashAnimation();
            }

            @Override
            public void onFailOfResponse(Object... arguments) {

            }
        };
        return mListener;
    }

    @SuppressLint("DefaultLocale")
    private void saveAllDrawableToStorage() {
        ArrayList<String> mTempImagePathArray = new ArrayList<String>();
        for (int i = 0; i < AppHelper.catholicImagesDrawableArray.length; i++) {
            Bitmap bm = BitmapFactory.decodeResource(getResources(),
                    AppHelper.catholicImagesDrawableArray[i]);
            String fileName = AppHelper.catholicNameArray[i].replaceAll(" ",
                    "_").toLowerCase();

            if (!AppDefaultUtils.isFilePresent(fileName)
                    || fileName.toLowerCase().contains("chalice")) {
                AppDefaultUtils.savePassengerBitmapToSDCard(bm, fileName);
            }
            mTempImagePathArray.add(fileName);
        }

        AppHelper.catholicImagesArray = new String[mTempImagePathArray.size()];
        AppHelper.catholicImagesArray = mTempImagePathArray
                .toArray(AppHelper.catholicNameArray);
    }

    private void moveToNextScreen() {
        startActivity(new Intent(SplashActivity.this, NameMatchScreen.class));
        finish();
    }


    /**
     * fetch Game Data from server
     */
    private void GetGameDataFromServer() {
        new MemoryMatchAPIHandler(SplashActivity.this, null);
    }

    // /**
    // * On API Response
    // *
    // * @return
    // */
    // private WebAPIResponseListener onAPIResponseListener() {
    // WebAPIResponseListener mListener = new WebAPIResponseListener() {
    //
    // @SuppressWarnings("unchecked")
    // @Override
    // public void onSuccessOfResponse(Object... arguments) {
    // reTryForLogin.setVisibility(View.GONE);
    // if (arguments != null && arguments.length > 0) {
    // imageArray = (ArrayList<String>) arguments[0];
    // fillArrayWithData(imageArray);
    // }
    // AppDefaultUtils.showProgressDialog(SplashActivity.this,
    // "Loading Game Data..", false);
    // }
    //
    // @SuppressWarnings("unchecked")
    // @Override
    // public void onFailOfResponse(Object... arguments) {
    // if (arguments != null && arguments.length > 0) {
    // imageArray = (ArrayList<String>) arguments[0];
    // fillArrayWithData(imageArray);
    // }
    // // manageAPIResponse();
    //
    // }
    // };
    // return mListener;
    // }

    // /**
    // * Manage API Response
    // */
    // public void manageAPIResponse() {
    // if (AppHelper.catholicNameArray != null
    // && AppHelper.catholicImagesArray != null
    // && (AppHelper.catholicNameArray.length ==
    // AppHelper.catholicImagesArray.length)
    // && AppHelper.catholicNameArray.length > 0) {
    //
    // } else {
    // reTryForLogin.setVisibility(View.VISIBLE);
    // }
    // // if (imageArray != null && imageArray.isEmpty()) {
    // // reTryForLogin.setVisibility(View.VISIBLE);
    // // } else {
    // // if (AppHelper.catholicNameArray != null
    // // && AppHelper.catholicImagesArray != null
    // // && (AppHelper.catholicNameArray.length ==
    // // AppHelper.catholicImagesArray.length)) {
    // // startActivity(new Intent(SplashActivity.this,
    // // ChooseNextActivity.class));
    // // finish();
    // // } else {
    // // reTryForLogin.setVisibility(View.VISIBLE);
    // // }
    // //
    // // }
    // }

    // /**
    // * Fill game data into array
    // *
    // * @param stockList
    // */
    // private void fillArrayWithData(ArrayList<String> stockList) {
    // AppHelper.catholicNameArray = new String[stockList.size()];
    // AppHelper.catholicNameArray = stockList
    // .toArray(AppHelper.catholicNameArray);
    //
    // }

}
